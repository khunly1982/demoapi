﻿using Cognitic.Tools.Security.Jwt.Configuration;
using Cognitic.Tools.Security.Jwt.Models;
using Cognitic.Tools.Security.Jwt.Services;
using System;

namespace Test.Jwt
{
    class User : IPayload
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }

        public DateTime BirthDate { get; set; }
        public string Username { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            User u = new User { Id = 42, 
                Username = "KhunLY", Email = "Lykhun@gmail.Com", Role = "Customer", BirthDate = new DateTime(1982, 5, 6) };

            JwtConfiguration config = new JwtConfiguration
            {
                Issuer = "my issuer",
                Audience = "my audience",
                Exp = 86400,
                Signature = "c!Nzy}BWu!x8U!nv%5n7P5QQRJUDutQD@#R-^#*^Z/L9?<.5q>",
                ValidateExp = true,
                ValidateAudience = true,
                ValidateIssuer = true,
            };
            
            JwtService service = new JwtService(config);

            Console.WriteLine(service.Encode(u));


            string t = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9lbWFpbGFkZHJlc3MiOiJMeWtodW5AZ21haWwuQ29tIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9wcmltYXJ5c2lkIjoiNDIiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJDdXN0b21lciIsImlkIjoiNDIiLCJlbWFpbCI6Ikx5a2h1bkBnbWFpbC5Db20iLCJyb2xlIjoiQ3VzdG9tZXIiLCJiaXJ0aGRhdGUiOiIwNi0wNS04MiAwMDowMDowMCIsInVzZXJuYW1lIjoiS2h1bkxZIiwibmJmIjoxNjIyNzE2NjYzLCJleHAiOjE2MjI3MTY2NjQsImlzcyI6Im15IGlzc3VlciIsImF1ZCI6Im15IGF1ZGllbmNlIn0.J6P9ws2XVaS4avSPgo94_ipvHCqCl-oeebDTZFuJ0dY";

            if(service.Decode(t) != null)
            {
                Console.WriteLine("Valide");
            }
            else
            {
                Console.WriteLine("Non Valide");
            }

            Console.ReadKey();
        }
    }
}
