﻿using Cognitic.Tools.Security.Jwt.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Cognitic.Tools.Security.Jwt.Middleware
{
    public class JwtMiddleware
    {
        private RequestDelegate _next;

        public JwtMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, JwtService service)
        {
            StringValues authorizations = context.Request.Headers.FirstOrDefault(h => h.Key == "Authorization").Value;
            string token = authorizations.FirstOrDefault(h => h.StartsWith("Bearer"))?.Replace("Bearer ", "");
            if (token != null)
            {
                ClaimsPrincipal principal = service.Decode(token);
                if (principal != null)
                {
                    context.User = principal;
                }
            }
            await _next.Invoke(context);
        }
    }
}
