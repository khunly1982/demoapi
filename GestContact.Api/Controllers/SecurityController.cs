﻿using Cognitic.Tools.Security.Jwt.Models;
using Cognitic.Tools.Security.Jwt.Services;
using GestContact.Api.Models.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestContact.Api.Controllers
{

    class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }

    class UserDTO : IPayload
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
    }

    [Route("api/[controller]")]
    [ApiController]
    public class SecurityController : ControllerBase
    {
        private static IEnumerable<User> fakeDB = new List<User>
        {
            new User { Id = 1, Email = "Lykhun@gmail.com",  Password = "1234", Role = "Customer" },
            new User { Id = 2, Email = "morre.t@gmail.com",  Password = "12345", Role = "Admin" },
        };

        private readonly JwtService _tokenService;

        public SecurityController(JwtService tokenService)
        {
            _tokenService = tokenService;
        }

        [HttpPost("login")]
        [Produces("application/json", Type = typeof(string))]
        public IActionResult Login([FromBody]LoginForm credentials)
        {
            // vérifier les credentials
            // et récupérer les infos de l'utilisateur
            User u = fakeDB.FirstOrDefault(us => 
                us.Email == credentials.Email && us.Password == credentials.Password
            );
            UserDTO dto = new UserDTO { Id = u.Id, Email = u.Email, Role = u.Role };
            // if valid
            if(u != null)
            {
                // encoder les infos dans un token
                string token = _tokenService.Encode(dto);
                // retourner le token
                return Ok(token);
            }
            // sinon
            else
            {
                // retourner une erreur
                return Unauthorized();
            }
        }
    }
}
